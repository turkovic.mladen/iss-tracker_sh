#!/bin/bash
# plot the ISS location on World Map

help='iss <OPTION>
get the location of ISS from http://api.open-notify.org/  
 and draw its location on the map of the world (+/- few 1000 kilometers :)
 
-h, --help       print this help
'

if [[ $1 = -h || $1 = --help ]]; then
   echo "$help"
   exit
fi

value=$(curl -s "http://api.open-notify.org/iss-now.json")
  
iss_datetime=$(date -d @$(echo $value | grep -Pom 1 '"timestamp": \K[0-9]*'))
iss_latitude=$(echo $value | grep -Pom 1 '"latitude": "\K[^"]*')
iss_longitude=$(echo $value | grep -Pom 1 '"longitude": "\K[^"]*')

echo "ISS position on" $iss_datetime

# remove negative sign if South
if [ ${iss_latitude%.*} -lt 0 ]; then
	echo " > latitude : ${iss_latitude:1} °S"
else
	echo " > latitude : $iss_latitude °N"
fi

# remove negative sign if West
if [ ${iss_longitude%.*} -lt 0 ]; then
	echo " > longitude: ${iss_longitude:1} °W"
else
	echo " > longitude: $iss_longitude °E"
fi

# ########################################### #
# from: http://www.asciiworld.com/-Maps-.html #
# ########################################### #

# inverted color & bold (if possible)
pos_on="\e[7m\e[1m"
pos_off="\e[0m"

world_map="
+---------------------------------------------------------------------+
|.              ,_   .  ._. _.  .                                     |
|           , _-\','|~\~      ~/      ;-'_   _-'     ,;_;_,    ~~-    |
|  /~~-\_/-'~'--' \~~| ',    ,'      /  / ~|-_\_/~/~      ~~--~~~~'--_|
|  /              ,/'-/~ '\ ,' _  , '|,'|~                   ._/-, /~ |
|  ~/-'~\_,       '-,| '|. '   ~  ,\ /'~                /    /_  /~   |
|.-~      '|        '',\~|\       _\~     ,_  ,               /|      |
|          '\        /'~          |_/~\\\\\,-,~  \ \"         ,_,/ |      |
|           |       /            ._-~'\_ _~|              \ ) /       |
|            \   __-\           '/      ~ |\  \_         /  ~         |
|  .,         '\ |,  ~-_      - |          \\\\\_' ~|  /\  \~ ,          |
|               ~-_'  _;       '\           '-,   \,' /\/  |          |
|                 '\_,~'\_       \_ _,       /'    '  |, /|'          |
|                   /     \_       ~ |      /          \  ~'; -,_.    |
|                   |       ~\        |    |  ,         '-_, ,; ~ ~\  |
|                    \,      /        \    / /|            ,-, ,   -, |
|                     |    ,/          |  |' |/          ,-   ~ \   '.|
|                    ,|   ,/           \ ,/              \       |    |
|                    /    |             ~                 -~~-, /   _ |
|                    |  ,-'                                    ~    / |
|                    / ,'                                      ~      |
|                    ',|  ~                                           |
|                      ~'                                             |
+---------------------------------------------------------------------+
"
# map goes to approx. 85°N and 65°S; 
# the eqiuator is located on line 13 (85 / 13 = 6.538)
line_on_map=$(( ( ( (90 - ${iss_latitude%.*})*100 / 654 ) ) ))

# map covers 360° in 70 columns i.e. (360 / 70 = 5.143)
column_on_map=$(( ( ( ( ${iss_longitude%.*} + 180 ) * 100 / 514 ) ) ))

pos_on_map=$(( ($line_on_map * 72) + $column_on_map))

# fixing extra length due to escape chars :(
if [ $pos_on_map -gt 545 ]; then
	pos_on_map=$(($pos_on_map + 1))
fi
if [ $pos_on_map -gt 766 ]; then
	pos_on_map=$(($pos_on_map + 1))
fi

# #################################### #
# output the position on the World Map #
# #################################### #

# echo "line_on_map: " $line_on_map
# echo "column_on_map: " $column_on_map
# echo "pos_on_map: " $pos_on_map

## get curent location char; to skip eol '\n' char
charLoc=${world_map:$pos_on_map:1}

## get previous char; to escape '\' char
charLocPrev=${world_map:$pos_on_map-1:1}

# echo -e $("$world_map" | sed s/$charLoc/+/$pos_on_map)

if [ "${charLoc-}" = $'\n' ]; then
	echo -e "${world_map:0:$pos_on_map+1}"$pos_on"+"$pos_off"${world_map:$pos_on_map+2}"
elif [ "${charLocPrev-}" = $'\\' ]; then
	echo -e "${world_map:0:$pos_on_map-1}""\ "$pos_on"+"$pos_off"${world_map:$pos_on_map+2}"
else
	echo -e "${world_map:0:$pos_on_map}"$pos_on"+"$pos_off"${world_map:$pos_on_map+1}"
fi
